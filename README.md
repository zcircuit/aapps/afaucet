> ### THIS REPO AND ITS CONTRACTS HAVE **NOT** BEEN AUDITED BY A THIRD-PARTY, see license.txt for usage details

# @zCircuit/Drip

> Simply put, a faucet for sustainably dripping funds from a reserve

This project's responsibility is to evenly (as possible) distribute a reserve of an asset or algos to an indeterminate number of receivers over an indeterminate amount of time.  The faucet is configured with a reward rate per second, and distributes those rewards, each second, to all accounts opted-in to the faucet.  An account is only able to join the faucet if it has been approved by the Faucet's approver. Once joining the faucet, accounts will be able to make a claim to receive their funds at any time interval they choose.

Possible use-cases:
 - Creating a 'mint' and 'burn' style asset economy
 - Doing an asset or algo giveaway

## Faucet User Lifecycle

 1. **Get approval from the faucet's configured Approver**.  The approver could be a normal account, it could be a LogicSig with specific rules for getting approval, or it could be a seperately deployed contract.
 1. **Opt-in to the faucet**. Now that the account has been approved it can simply opt-in and begin accumulating a distribution of the faucet funds.
 1. **Claim accrued faucet funds**. Although accounts begin accumulating immediately it is the responsibility of each account to claim their funds at any time.

## How to interact with an existing faucet

Add the `@zcircuit/drip` package to your projects dependencies.  This package is not published to npmjs.org so you will need to configure your `@zcircuit` npm registry.

1. Create a `.npmrc` file in your project at the same level as your `package.json` (If you don't already have one). Add the following to your `.npmrc`
    ```
    @zcircuit:registry=https://gitlab.com/api/v4/projects/35806759/packages/npm/
    ```

1. Add the `@zcircuit/drip` package to your project
    ```bash
    npm install @zcircuit/drip
    ```

1. Construct an instance of the SDK, providing an algod client, indexer client, faucet details, and a strategy for signing transactions
    ```ts
    import { DripSdk } from '@zcircuit/drip';

    const dripSdk = new DripSdk(
        algod,
        indexer,
        faucetAppId,
        faucetAssetId,
        signer,
    );
    ```

> For more advanced use-cases where you need to contruct your own transaction groups, use the DripTransactions class
```ts
import { DripTransactions } from '@zcircuit/drip';

const dripTxns = new DripTransactions(
    faucetAppId,
    faucetAssetId,
);
```

## How to deploy your own faucet

> Again, these contracts have NOT been security audited, refer to license.txt for usage details
>   - We do ask that if you perform an audit of this code for your own purposes, please share the results publicly and we can include the audit details with the repo

1. Follow the steps above to configure the `@zcircuit` npm registry
1. Add the `@zcircuit/drip-deployer` package to your project
    ```bash
    npm install @zcircuit/drip-deployer
    ```
1. Use the exported `deployDrip` method
   - This package also includes a `getDeployDripTransaction` to get just the deploy transaction, as well as a couple other helpful functions

**Deploy Configuration**
```ts
interface DripDeployConfig {
    assetId: bigint,                // The asset the faucet will distribute
    approverAddr: string,           // The address of an account responsible for approving faucet users
    rewardRate: bigint,             // A number (x/1,000,000), that represents what fraction of the current reserve should be distributed every second
    maxClaimAccrualSecs: bigint,    // The max time a user can accumulate rewards
    maxClaimPercent: bigint,        // A number (x/1,000,000), that represents what fraction of the current reserve can be claimed by a single user at any time
}
```

## Repo Introduction

This repo contains the source for 2 modules: `@zcircuit/drip` & `@zcircuit/drip-deployer`. Both packages provide typescript type declarations. The repo is organized into 3 source folders:

 - `./algorand`: The source for the Algorand pyTeal smart contract implementations.  Also included is an extensive test suite that demonstrates and ensures expected contract behaviour. These tests make use of the Drip SDK which also demonstrates how to use the SDK effectively.
 - `./deployer`: The source for `@zcircuit/drip-deployer`. This module can be used to configure and deploy your own instance of a Drip faucet to the Algorand blockchain without requiring the pyTeal environment setup.
 - `./sdk`: The source for `@zcircuit/drip`. This module is the Drip SDK which can be used to interact with any instance of a deployed Drip.

## Rewards Calculation

Ideally rewards would be accumulated continuously to get the most accurate and uniform distribution. However, Algorand does not have a mechanism for scheduling interactions over an interval indefinately.  To solve for this we must calculate rewards over the time distribution within some level of precision. The easiest time interval we can measure on-chain is the time difference between two claims. Additionally, over this time slice the number of faucet participants will change and the faucet's reserve will be changing.  These fluctuations are lost information when calculating rewards for a claim.  However we can help mitigate the impact by averaging the user's reward rate over the time slice. So let's look at some math.

```math
r = \textrm{Developer configured rewards coefficient per second rate} \\
p_t = \textrm{Num Participants at a given time} \\
C = \textrm{Current (reserve) balance of the faucet at claim time} \\
d = \textrm{Time Delta in seconds between claims that we're calculating rewards for} \\
\textrm{...}\\

RPS = \textrm{Percent of Reserve Released Per Second} =  \frac{r}{1000000} \\
\textrm{ }\\
\textrm{ }\\

R_t = \textrm{Per Account Reward Rate At Any Time} = \frac{RPS}{p_t} = \frac{r}{1000000p_t} \\
\textrm{ }\\
\textrm{ }\\

R_1 = \textrm{Reward Rate At Most Recent Claim Time}\\
R_2 = \textrm{Reward Rate At Claim Time}\\
p_1 = \textrm{Number of Participants At Most Recent Claim Time}\\
p_2 = \textrm{Number of Participants At Claim Time}\\
\textrm{ }\\
\textrm{ }\\

\textrm{Average Reward Rate over time range} = \frac{R_1 + R_2}{2} \\
\textrm{ }\\
\textrm{ }\\

Rewards = \frac{R_1 + R_2}{2}*d*C \\
\textrm{ }\\
\darr \\
\textrm{ }\\

Rewards = \frac{\frac{r}{1000000p_1} + \frac{r}{1000000p_2}}{2}*d*C \\
\textrm{ }\\
\darr \\
\textrm{ }\\

Rewards = \frac{d*r*C*(p_1 + p_2)}{2000000*p_1*p_2}
```


