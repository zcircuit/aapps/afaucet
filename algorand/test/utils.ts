import { getProgram } from "@algo-builder/runtime";
import {
    Account,
    Algodv2,
    generateAccount,
    Indexer,
    makeApplicationCallTxnFromObject,
    makeApplicationCreateTxnFromObject,
    makeAssetCreateTxnWithSuggestedParamsFromObject, makeAssetTransferTxnWithSuggestedParamsFromObject,
    makePaymentTxnWithSuggestedParamsFromObject,
    mnemonicToSecretKey,
    OnApplicationComplete,
    waitForConfirmation
} from "algosdk";
import { CreateApplicationTransactionQuery } from "./types";

const clearProgram = getProgram('faucet-clear.py');

export class IUtils {

    algod: Algodv2;
    indexer: Indexer;

    private master: Account;

    constructor(algod?: Algodv2, indexer?: Indexer, mnemonic?: string) {
        this.algod = algod || new Algodv2(
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'http://localhost',
            4001,
        );
        this.indexer = indexer || new Indexer(
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'http://localhost',
            8980,
        );
        const defaultMnemonic = 'enforce drive foster uniform cradle tired win arrow wasp melt cattle chronic sport dinosaur announce shell correct shed amused dismiss mother jazz task above hospital';
        this.master = mnemonicToSecretKey(mnemonic || defaultMnemonic);
    }

    createFundedAccounts(num: number) {
        return Promise.all(Array.from({ length: num })
            .map(async () => {
                const acc = generateAccount();

                await this.sendAlgos(this.master, acc.addr, 10e6);
                return acc;
            })
        )
    }

    createTestingASAs(creators: Account[]) {
        return Promise.all(creators.map(async (account, i) => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const tx = makeAssetCreateTxnWithSuggestedParamsFromObject({
                decimals: 0,
                defaultFrozen: false,
                total: 1e6 * 10,
                from: account.addr,
                assetName: `ASA-${i}`,
                suggestedParams,
            })
            const signedTx = tx.signTxn(account.sk);
            await this.algod.sendRawTransaction(signedTx).do();
            await waitForConfirmation(this.algod, tx.txID(), 20);

            const createdAssetTransaction: any = await this.tryQueryForTransaction(tx.txID());
            const assetId = createdAssetTransaction.transaction["created-asset-index"];

            console.log('Asset Created', assetId);
            return BigInt(assetId);
        }));
    }

    async sendAlgos(from: Account, toAddr: Account['addr'], amountMicroAlgos: number) {
        const suggestedParams = await this.algod.getTransactionParams().do();

        const tx = makePaymentTxnWithSuggestedParamsFromObject({
            amount: amountMicroAlgos,
            from: from.addr,
            to: toAddr,
            suggestedParams,
        });
        const signedTx = tx.signTxn(from.sk);
        await this.algod.sendRawTransaction(signedTx).do();
        await waitForConfirmation(this.algod, tx.txID(), 20);
    }

    async sendAsset(assetId: bigint, from: Account, toAddr: Account['addr'], amount: number) {
        const suggestedParams = await this.algod.getTransactionParams().do();

        const tx = makeAssetTransferTxnWithSuggestedParamsFromObject({
            assetIndex: Number(assetId),
            amount,
            from: from.addr,
            to: toAddr,
            suggestedParams,
        });
        const signedTx = tx.signTxn(from.sk);
        await this.algod.sendRawTransaction(signedTx).do();
        await waitForConfirmation(this.algod, tx.txID(), 20);
    }

    async optInAssetIfNecessary(account: Account, assetId: bigint) {
        const result = await this.indexer.lookupAccountByID(account.addr).do();
        const hasAsset = (result.account.assets || []).some((t: any) => t['asset-id'] === assetId);
        if (hasAsset) return;

        await this.sendAsset(assetId, account, account.addr, 0);
        console.log(`${account.addr} opt-in to ${assetId}`);
    }

    async deployFaucet(
        creator: Account,
        assetId: bigint,
        approverAddr: string,
        config = {
            rewardRate: (1000000n) / 100n,
            maxClaimAccumulationSeconds: 60n,
            maxClaimPercent: 1000000n, // 100%
        }
    ) {
        const templatedFaucetProgram = getProgram('faucet.py');
        const templateConfig = {
            'ASSET_ID': assetId,
            'APPROVER_ADDR': approverAddr,
            'REWARD_RATE': config.rewardRate,
            'MAX_CLAIM_ACC': config.maxClaimAccumulationSeconds,
            'MAX_CLAIM_PERCENT': config.maxClaimPercent,
        };

        const deTemplatedFaucetTeal = Object.entries(templateConfig).reduce((templatedTeal, [key, value]) => {
            return templatedTeal.replace(new RegExp(`TMPL_${key}`, 'g'), value.toString());
        }, templatedFaucetProgram);

        let suggestedParams = await this.algod.getTransactionParams().do();
        const compiledClearProgram = await (this.algod.compile(clearProgram).do() as CompileResult);
        const compiledFaucetProgram = await (this.algod.compile(deTemplatedFaucetTeal).do() as CompileResult);

        const faucetTx = makeApplicationCreateTxnFromObject({
            approvalProgram: b64ToUint8(compiledFaucetProgram.result),
            clearProgram: b64ToUint8(compiledClearProgram.result),
            numGlobalInts: 1,
            numGlobalByteSlices: 1,
            numLocalInts: 2,
            numLocalByteSlices: 0,
            onComplete: OnApplicationComplete.NoOpOC,
            from: creator.addr,
            suggestedParams,
        });

        const signedFaucetTx = faucetTx.signTxn(creator.sk);
        await this.algod.sendRawTransaction(signedFaucetTx).do();
        await waitForConfirmation(this.algod, faucetTx.txID(), 20);

        const createdFaucetTransaction: CreateApplicationTransactionQuery = await this.tryQueryForTransaction(faucetTx.txID());
        const faucetAppId = createdFaucetTransaction.transaction["created-application-index"];
        console.log(`Faucet Application created: ${faucetAppId}`);
        return faucetAppId;
    }

    async lookupBalance(address: string, assetId = 0n): Promise<number> {
        await this.waitForAlgodIndexerSync();
        const accountDetails = await this.indexer.lookupAccountByID(address).do();
        const asset = (accountDetails.account.assets || []).find((a: any) => BigInt(a['asset-id']) === assetId);

        return !!assetId
            ? asset?.amount || 0
            : accountDetails.account.amount;
    }

    async waitForChainTimeAfter(timeMs: number, timeout = 30000) {
        const start = Date.now();
        const waitingTime = Math.floor(timeMs / 1e3);
        while (true) {
            const indexerStatus = await this.indexer.makeHealthCheck().do();
            const blockInfo = await this.indexer.lookupBlock(indexerStatus.round).do();
            const blockTime = blockInfo.timestamp;
            if (blockTime > waitingTime) break;
            if (Date.now() - start > timeout) throw new Error('TIMEOUT');
            await tick(2000);
        }
    }

    async waitForIndexerRound(roundToWaitFor: number) {
        while (true) {
            const { round } = await this.indexer.makeHealthCheck().do();
            if (round >= roundToWaitFor) break;
            await tick(500);
        }
    }

    async waitForAlgodIndexerSync() {
        const algodRes = await this.algod.status().do();
        const roundToWaitFor = algodRes['last-round'];
        console.log('Waiting for round:', roundToWaitFor);

        await this.waitForIndexerRound(roundToWaitFor);
    }

    async getTxConfirmationTimestamps(...confirmations: any[]) {
        // Note: In pyTeal Global.latestTimestamp will refer to timestamp of the previous round
        //  - I would expect to need a round-1 to find the correct timestamp but this test still
        //    passes without it.  Maybe a sandbox quirk? Maybe rounds don't literally increment with a +1?
        const rounds = confirmations.map(c => c['confirmed-round'] - 1);
        const roundInfo = await Promise.all(rounds.map(r => this.indexer.lookupBlock(r).do()));

        return roundInfo.map(r => r.timestamp);
    }

    private async tryQueryForTransaction(txid: string, retries = 10) {
        let i = 0;
        let lastError: any;
        while (i < retries) {
            const createdTransaction: CreateApplicationTransactionQuery | null = await this.indexer.lookupTransactionByID(txid).do()
                .catch(e => {
                    lastError = e;
                    return null;
                }) as any;

            if (createdTransaction) return createdTransaction;
            await tick(1000);
            i++;
        }

        throw new Error(lastError.toString());
    }
}

export function tick(time: number) {
    return new Promise(res => setTimeout(res, time));
}

export const atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

export const btoa = globalThis.btoa || ((src: string) => {
    return Buffer.from(src, 'binary').toString('base64');
})

export function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}

export function textToUint8(text: string): Uint8Array {
    return Uint8Array.from(text, c => c.charCodeAt(0));
}

export function uint8ToText(bytes: Uint8Array): string {
    return Array.from(bytes).map(b => String.fromCharCode(b)).join('');
}

export function uint8ToB64(bytes: Uint8Array): string {
    return btoa(uint8ToText(bytes));
}

export function fees(numTxs: number) {
    return numTxs * 1e3;
}

export function algos(numAlgos: number) {
    return numAlgos * 1e6; //microAlgos
}

export function seconds(numSeconds: number) {
    return numSeconds * 1e3; //milliSeconds
}

export async function retryable<T>(tries: number, fn: () => Promise<T>): Promise<T> {
    let iter = 0;
    let err;
    let result: T = null as any;

    while (iter < tries) {
        iter++;
        err = null;

        result = await fn().catch(e => err = e);
        if (err) {
            await tick(1000);
        } else {
            break;
        };
    }

    if (err) throw err;

    return result;
}

const defaultWithError = () => { }
export async function assertRejected(promise: Promise<unknown>, withError: (e: Error) => void = defaultWithError) {
    let caught = false;

    await promise.catch(e => {
        caught = true;
        withError(e);
    });

    if (!caught) throw new Error('Expected a rejection');
}

export type CompileResult = Promise<{
    hash: string,
    result: string; //base64 encoded
}>

export function expectedRewards(
    startTimeSecs: bigint,
    endTimeSecs: bigint,
    startParticipants: bigint,
    endParticipants: bigint,
    currentReserve: bigint,
    rewardRateNumerator: bigint,
) {
    const div = (top: bigint, bot: bigint) => top / bot; // For readibility

    const timeDelta = endTimeSecs - startTimeSecs;
    return div(
        timeDelta * rewardRateNumerator * (startParticipants + endParticipants) * currentReserve,
        BigInt((1e6) * 2) * startParticipants * endParticipants
    );
}
