import type { Algodv2, Indexer, Transaction } from 'algosdk';
import { AlgoSdk } from './algosdk';
import { DripTransactions } from './transactions';
import { atob, textToUint8 } from './utils';

const ALGO_ASSET_ID = 1n;

export class DripSdk {

    private transactions = new DripTransactions(this.algosdk, this.appId, this.faucetAssetId);

    constructor(
        private algosdk: AlgoSdk,
        private algod: Algodv2,
        private indexer: Indexer,
        private appId: bigint,
        private signer: (txns: Transaction[]) => Promise<Uint8Array[]>,
        private faucetAssetId: bigint = ALGO_ASSET_ID,
    ) { }

    optIn(address: string) {
        return this.withErrorHandling('[Drip: optIn]-', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();

            const tx = this.transactions.optInToDrip(address, suggestedParams);

            let txns = [tx];
            if (await this.needsOptIn(address, this.faucetAssetId)) {
                const optInTx = this.transactions.optInToAsset(address, suggestedParams);

                txns = this.algosdk.assignGroupID([optInTx, ...txns]);
            }

            const signedTxns = await this.signer(txns);
            await this.algod.sendRawTransaction(signedTxns).do();
            return await this.algosdk.waitForConfirmation(this.algod, tx.txID(), 20);
        });
    }

    approveAccount(approverAddr: string, addrToApprove: string) {
        return this.withErrorHandling('[Drip: approveAccount]-', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const tx = this.transactions.approveAccount(approverAddr, addrToApprove, suggestedParams);
            const [signedTx] = await this.signer([tx]);
            await this.algod.sendRawTransaction(signedTx).do();
            await this.algosdk.waitForConfirmation(this.algod, tx.txID(), 20);
        });
    }

    claim(claimerAddr: string) {
        return this.withErrorHandling('[Drip: claim]-', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const tx = this.transactions.claim(claimerAddr, suggestedParams);
            const [signedTx] = await this.signer([tx]);
            await this.algod.sendRawTransaction(signedTx).do();
            return await this.algosdk.waitForConfirmation(this.algod, tx.txID(), 20);
        });
    }

    optOut(address: string) {
        return this.withErrorHandling('[Drip: optOut]-', async () => {
            const suggestedParams = await this.algod.getTransactionParams().do();
            const tx = this.transactions.optOut(address, suggestedParams);
            const [signedTx] = await this.signer([tx]);
            await this.algod.sendRawTransaction(signedTx).do();
            return await this.algosdk.waitForConfirmation(this.algod, tx.txID(), 20);
        });
    }

    async triggerOptInToAsset(senderAddr: string, forcedFee?: number) {
        let suggestedParams = await this.algod.getTransactionParams().do();
        const tx = this.algosdk.makeApplicationCallTxnFromObject({
            appIndex: Number(this.appId),
            from: senderAddr,
            onComplete: this.algosdk.OnApplicationComplete.NoOpOC,
            suggestedParams: {
                ...suggestedParams,
                flatFee: true,
                fee: forcedFee || Math.max(suggestedParams.fee, 1e3) * 2,
            },
            appArgs: [
                textToUint8('aoptin')
            ],
            foreignAssets: [
                Number(this.faucetAssetId)
            ]
        });

        const [signedTx] = await this.signer([tx]);
        await this.algod.sendRawTransaction(signedTx).do();
        return await this.algosdk.waitForConfirmation(this.algod, tx.txID(), 20);
    }

    async getAccountState(account: string) {
        const appLocalState = (await this.indexer.lookupAccountAppLocalStates(account).do())['apps-local-states']
            .find((ls: any) => ls.id === Number(this.appId));

        const decodedKeys = Object.fromEntries(appLocalState['key-value'].map((kv: any) => [atob(kv.key), kv.value]));

        return {
            lastClaim: decodedKeys.lastClaim.uint as number,
            lastNumP: decodedKeys.lastNumP.uint as number,
        };
    }

    private needsOptIn(address: string, assetId: bigint) {
        return this.withErrorHandling('[Drip: needsOptIn]-', async () => {
            if (assetId === ALGO_ASSET_ID) return false;

            const numAssetId = Number(assetId);

            const lookupResult = await this.indexer.lookupAccountByID(address).do();
            return !((lookupResult.account.assets || []).some((t: any) => t['asset-id'] === numAssetId));
        });
    }

    private async withErrorHandling<T = unknown>(desc: string, todo: () => Promise<T>): Promise<T> {
        try {
            return await todo();
        } catch (e: any) {
            // TODO: Use 'debug' package strategy
            console.error(`Error:${desc}[${e.status}]`, e?.response?.text);
            throw e;
        }
    }
}
